# LAMMPS Output File Reader

##### LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator

### To do:  
- add sample code for how to use
### This is to read the text output of LAMMPS, not dcd binary format.
### Currently Support:  
* reading one single frame
* reading frame at fixed intervals
* conversion of atomic coordinates from/to relative and absolute
* conversion of atomic coordinates from/to wrapped and unwrapped
* selecting specific atoms using ID, i.e., integer
* selecting all atoms of element(s) through element ID, integer or string, e.g., 'Si' for silicon

### Planned Features  
* Blender support
