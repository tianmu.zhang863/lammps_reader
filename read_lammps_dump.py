##################################
## Read Lammps dumped files
## Tianmu Zhang, Ruhil Dongol
## Materials Design and Innovation
## University at Buffalo
## {tzhang4, }@buffalo.edu
## tianmu.zhang863@gmail.com
## ruhilma528@gmail.com

import numpy as np
import pandas as pd
import warnings

def read_lammps_box_cubic(in_str):
    '''
    Read the bounding box of the simulation, cubic system.
    Return is a list of three lists for x,y and z, each of the three list has three elements:
    lower_limit, upper_limit and length.
    The information of the bounding box in the LAMMPS output usually contains
    three consecutive lines, this function assumes three lines in total, and the
    'fp' should be at the line of box indicator.
    '''
    if len(in_str) == 3:
        # xlmts = fp.readline().split()
        xlmts = in_str[0].split()
        xlmts = (float(xlmts[0]), float(xlmts[1]))
        box_x_len = np.abs(xlmts[0] - xlmts[1])
        # ylmts = fp.readline().split()
        ylmts = in_str[1].split()
        ylmts = (float(ylmts[0]), float(ylmts[1]))
        box_y_len = np.abs(ylmts[0] - ylmts[1])
        # zlmts = fp.readline().split()
        zlmts = in_str[2].split()
        zlmts = (float(zlmts[0]), float(zlmts[1]))
        box_z_len = np.abs(zlmts[0] - zlmts[1])
        box_dims = [[xlmts[0], xlmts[1], box_x_len],
                    [ylmts[0], ylmts[1], box_y_len],
                    [zlmts[0], zlmts[1], box_z_len]]
        return box_dims
    else:
        raise ValueError('The length of the input box dimension list is not 3,\
                        possibly using non-cubic box for cubic box.')

def read_lammps_atmpos_line(line, pos_mod, box_dims, wrapped_out, scaled_out, pos_idx, ixyz,):
    '''
    line: the current line of the file being read, this line should be a list, use .split()
    pos_mod: unscale, scaled, unwrapped, scale unwrapped
    pos_idx: a list of x, y and z position in the current line, 0, 1, 2 correspond
    to x y and z.
    ixyz: unit cell displacement index, needed when coverting from wrapped to unwrapped
    positions. Set to boolean False if cannot be found in the file,
    box_dims: the current box dimensions, a list of three lists, in each [lower, uppper, length]
    The length asked for here is redandant, but since the code is already written.
    '''
    # get xyz coordinates from the line
    atmposscl = np.array(\
    [line[pos_idx[0]], line[pos_idx[1]], line[pos_idx[2]]], dtype=float)
    # parsing the box dimension information, so the rest of the code is more readable.
    xlim_l = box_dims[0][0]
    xlim_h = box_dims[0][1]
    box_x_len = box_dims[0][2]
    ylim_l = box_dims[1][0]
    ylim_h = box_dims[1][1]
    box_y_len = box_dims[1][2]
    zlim_l = box_dims[2][0]
    zlim_h = box_dims[2][1]
    box_z_len = box_dims[2][2]

    if ixyz: # if ix, iy, iz exist
        ixyz_arr = np.array(\
        [line[ixyz[0]], line[ixyz[1]], line[ixyz[2]]], dtype=float)
    else: # if not treat all as 0,
        ixyz_arr = np.array([0, 0, 0])

    # Choose mode to read, based on input and output
    ###################################################
    # unscaled (wrapped)
    # for unscale input, first test if output is unwrapped.
    # then test if output is scale and unwrapped .
    ###################################################
    if pos_mod == 'unscale': # and wrapped
        # Since it is unscale, so the main if-else is based on whether is output is wrapped.
        if not wrapped_out: # test if output remains unscaled but need to be unwrappeds
            atmposscl[0] = atmposscl[0] + (ixyz_arr[0]*box_x_len)
            atmposscl[1] = atmposscl[1] + (ixyz_arr[1]*box_y_len)
            atmposscl[2] = atmposscl[2] + (ixyz_arr[2]*box_z_len)
        if scaled_out: # test if output is needs to be scaled,
            # warnings.warn('scaled mode current not available for unscale input data.')
            atmposscl[0] = (atmposscl[0]-xlim_l)/box_x_len
            atmposscl[1] = (atmposscl[1]-ylim_l)/box_y_len
            atmposscl[2] = (atmposscl[2]-zlim_l)/box_z_len
    ###################################################
    # end of unscaled
    ###################################################

    ###################################################
    # scaled (wrapped)
    ###################################################s
    if pos_mod == 'scaled':
        if not wrapped_out: # test if output remains unscaled but need to be unwrappeds
            atmposscl[0] = atmposscl[0] + ixyz_arr[0]
            atmposscl[1] = atmposscl[1] + ixyz_arr[1]
            atmposscl[2] = atmposscl[2] + ixyz_arr[2]

        if not scaled_out:
            atmposscl[0] = atmposscl[0]*box_x_len + xlim_l
            atmposscl[1] = atmposscl[1]*box_y_len + ylim_l
            atmposscl[2] = atmposscl[2]*box_z_len + zlim_l
    ###################################################
    # end of scaled
    ###################################################

    ###################################################
    # unwrapped (unscaled)
    ###################################################s
    if pos_mod == 'unwrapped':
        if wrapped_out:
            atmposscl[0] = (((atmposscl[0]-xlim_l)/box_x_len)%1)*box_x_len + xlim_l
            atmposscl[1] = (((atmposscl[1]-ylim_l)/box_y_len)%1)*box_y_len + ylim_l
            atmposscl[2] = (((atmposscl[2]-zlim_l)/box_z_len)%1)*box_z_len + zlim_l
        if scaled_out:
            atmposscl[0] = (atmposscl[0]-xlim_l)/box_x_len
            atmposscl[1] = (atmposscl[1]-ylim_l)/box_y_len
            atmposscl[2] = (atmposscl[2]-zlim_l)/box_z_len
            # if wrapped_out:
            #     atmposscl[0] = atmposscl[0]%1
            #     atmposscl[1] = atmposscl[1]%1
            #     atmposscl[2] = atmposscl[2]%1
    ###################################################
    # end of unwrapped
    ###################################################

    ###################################################
    # scaled unwrapped
    ###################################################s
    if pos_mod == 'scaleunwrapped':
        if wrapped_out:
            atmposscl[0] = atmposscl[0]%1
            atmposscl[1] = atmposscl[1]%1
            atmposscl[2] = atmposscl[2]%1
        if not scaled_out:
            atmposscl[0] = atmposscl[0]*box_x_len + xlim_l
            atmposscl[1] = atmposscl[1]*box_y_len + ylim_l
            atmposscl[2] = atmposscl[2]*box_z_len + zlim_l
    ###################################################
    # end of scaled unwrapped
    ###################################################
    return atmposscl

######################################################
# function to read one frame, this is used to record
# a dictionary of elements with the atom ID's that
# belong to the elements.
def list_ele_atoms(file_to_read):
    '''
    Read the lammpstrj file, for one frame, and record a dictionary of elements
    with the atoms belong to the elements.
    The keys of the dictionary is determined by the lammpstrj file, nanmely,
    the can be either 1, 2, 3,... or 'C', 'Si', 'Na'...
    This function can be used standalone, or
    the 'read_lammp_atoms' function will call this function (at this moment we
    do not consider the 'read_lammps_atoms' to read it, will incorporate this
    in later versions. Users need to manullay conbine the information from the
    outputs of the two funcions.).
    Return a dictionary of keys=elements, items list of atoms ID's.
    '''
    # Check the file ending, if not lammpstrj, will continue
    if file_to_read.split('.')[-1] != 'lammpstrj':
        warnings.warn('The file ending is not standard LAMMPS format.')
    # set read flow control flags
    rd_time_flag = False
    rd_time_flag2 = False
    rd_natm_flag = False

    # open file
    with open(file_to_read, 'r') as fp:
        # set counters
        rd_atom_ctr = 0
        # line = True # set line to True so can enter while loop.
        # read first line
        line = fp.readline()
        while line: # condition for line, this should go all the way to the last line.
            # read first line
            # split the string
            line = line.split()
            if 'ITEM:' in line: # look for if the line contains "ITEM "
                itm_pos = line.index('ITEM:')   # get the 'ITEM' index, this is done in
                                                # case spaces or other things exist before it.
                # condition for if this is for timestep
                if (line[itm_pos+1] == 'TIMESTEP') and (not rd_time_flag):
                    rd_time_flag = True # set flag
                if (line[itm_pos+1] == 'TIMESTEP') and rd_time_flag2: # finished one entire timeframe
                    break # break the while when reach the line of 'TIMESTEP'
                            # in the next time frame.
                # condition for total number of atoms in the frame, this
                if (line[itm_pos+1] == 'NUMBER') and (not rd_natm_flag) and rd_time_flag:
                    n_atm = int(fp.readline().split()[0])
                    rd_natm_flag = True
                # condition for atom list
                if (line[itm_pos+1] == 'ATOMS') and \
                    (rd_natm_flag) and (rd_time_flag):
                    try: # try to get the position of the atom_id
                        atom_id_pos = line[itm_pos+2:].index('id')
                    except: # this should not happen wiht LAMMPS output, but just in case
                        warnings.warn('Cannot find id field for atom. stop reading file')
                        break
                    try: # there are two possible ways used by LAMMPS for representing elements
                        atom_type_pos = line[itm_pos+2:].index('type') # can be 'type', this will be an integer later
                    except:
                        try:
                            atom_type_pos = line[itm_pos+2:].index('element') # or can be element, this will be a string, e.g., 'Si'
                        except:
                            warnings.warn('Cannot find type field for atom. stop reading file')
                            break

                    ele_atm_ls = {} # initilize dictionary for storage
                    for rdd in range(n_atm): # iterate one frame
                        line = fp.readline().split()
    #                     if int(line[1]) == ele_to_rd:
                        try:
                            crt_ele = int(line[atom_type_pos])
                        except:
                            crt_ele = line[atom_type_pos]
                        if crt_ele not in ele_atm_ls:
                            ele_atm_ls[crt_ele] = [int(line[atom_id_pos])]
                        elif crt_ele in ele_atm_ls:
                            ele_atm_ls[crt_ele] = ele_atm_ls[crt_ele] + [int(line[atom_id_pos])]
                    rd_time_flag2 = True # set to flag has gone through a whole time frame
            line = fp.readline()
    return ele_atm_ls

def read_lammps_atoms(file_to_read, atom_to_read=None, element_to_read=None, every_time_frames=1,\
                        wrapped_out=False, scaled_out=False, extra_ppt=[]):

# def read_lammps_element(file_to_read, element_to_read=[], time_frames_skip=100,\
#                         wrapped_out=False, scaled_out=False, extra_ppt=[]):
    '''
    Read atomic information, xyz coordinates.
    atom_to_read, list: the id's of the atoms to be read, this is to read
    a specific list of id's of atoms.
    element_to_read, list: the id's of the elements to be read, this is to read
    all of the atom of a type of element.
    If atom_to_read is not None, element to read will be ignored.
    every_time_frames: int, read every ## time frame, default is 1, to read every
    time step.
    wrapped_out: boolean, whether the output of the atom posiotions are in
    the unit cell.
    scaled_out: boolen, whether the output positions of the atoms are scaled.
    extra_ppt: properties other than the positions to be read. list of strings.
    Identifiers for this should be the same as the LAMMPS identifiers.

    Order of operation: read time --> read number of atoms --> read box dimensions
    ---> read atom coordinates and properties.
    This order is based on the standard LAMMPS output, there is flow control
    built in.

    Below is the  list from LAMMPS document, not all are suitble for using
    extra_ppt.
    id = atom ID
    mol = molecule ID
    proc = ID of processor that owns atom
    procp1 = ID+1 of processor that owns atom
    type = atom type
    element = name of atom element, as defined by dump_modify command
    mass = atom mass
    x,y,z = unscaled atom coordinates
    xs,ys,zs = scaled atom coordinates
    xu,yu,zu = unwrapped atom coordinates
    xsu,ysu,zsu = scaled unwrapped atom coordinates
    ix,iy,iz = box image that the atom is in
    vx,vy,vz = atom velocities
    fx,fy,fz = forces on atoms
    q = atom charge
    mux,muy,muz = orientation of dipole moment of atom
    mu = magnitude of dipole moment of atom
    radius,diameter = radius,diameter of spherical particle
    omegax,omegay,omegaz = angular velocity of spherical particle
    angmomx,angmomy,angmomz = angular momentum of aspherical particle
    tqx,tqy,tqz = torque on finite-size particles
    c_ID = per-atom vector calculated by a compute with ID
    c_ID[I] = Ith column of per-atom array calculated by a compute with ID, I can include wildcard (see below)
    f_ID = per-atom vector calculated by a fix with ID
    f_ID[I] = Ith column of per-atom array calculated by a fix with ID, I can include wildcard (see below)
    v_name = per-atom vector calculated by an atom-style variable with name
    d_name = per-atom floating point vector with name, managed by fix property/atom
    i_name = per-atom integer vector with name, managed by fix property/atom
    '''
    # Check the file ending, if not lammpstrj, will continue
    if file_to_read.split('.')[-1] != 'lammpstrj':
        warnings.warn('The file ending is not standard LAMMPS format.')

    # Check atom and element list and determine read mode
    try: # if atom_to_read list exist
        atom_to_read = list(atom_to_read)
        read_line_mode = 'atm'
        # initialize arrays and dictionary for storing
        time_arr = []
        pos_arr = {}
        ppt_arr = {}
        for atm in atom_to_read:    # initilize a list for storing coordinates for all of the atoms in list.
                                    # Consider change to dictionary for large amount of data purposes.
            pos_arr[atm] = []
            ppt_arr[atm] = []
    except:
        if atom_to_read==None: # if not, then
            try: # if element to read list exists
                element_to_read = list(element_to_read)
                read_line_mode = 'ele'
                time_arr = []
                pos_arr = {}
                ppt_arr = {}
                for ele in element_to_read:
                    pos_arr[ele] = []
                    ppt_arr[ele] = []
            except: # if both atom and element list are missing
                if element_to_read == None:
                    raise TypeError(r'Input for atom/element list not valid.')
        else:
            raise TypeError('atom_to_read cannot be coverted to list ')
    # set read flow control flags
    rd_pos_flag = False
    rd_time_flag = False
    box_dim_flag = False
    rd_natm_flag = False
    time_step_counter = 0

    # open file
    with open(file_to_read, 'r') as fp:
        # set counters
        rd_atom_ctr = 0
        time_skip_ctr = 0
        # line = True # set line to True so can enter while loop.
        # read first line
        line = fp.readline()
        while line: # condition for line, this should go all the way to the last line.
            # read first line
            # split the string
            line = line.split()
            if 'ITEM:' in line: # look for if the line contains "ITEM "
                itm_pos = line.index('ITEM:')   # get the 'ITEM' index, this is done in
                                                # case spaces or other things exist before it.
                # condition for if this is for timestep
                if (line[itm_pos+1] == 'TIMESTEP') and (not rd_time_flag):
                    ####################################
                    ####################################
                    ####################################
                    # Need to debug for this if condition,
                    # if every_time_frame is set to 10,
                    # ValueError: invalid literal for int() with base 10: 'ITEM:'
                    # no actual change in the code except this comment, will
                    # commit this comment in git and fix bug later.
                    time_skip_ctr += 1 # add timestep_skip
                    if time_skip_ctr==1 and time_step_counter==0:
                        current_time = int(fp.readline().split()[0]) # record current
                        rd_time_flag = True # set flag
                        time_step_counter=1
                    if time_skip_ctr >= every_time_frames and time_step_counter==1:
                        # only record time step and set flag at every # of time_frame_skip
                        # time_arr.append(int(fp.readline().split()[0])) # record time
                        current_time = int(fp.readline().split()[0]) # record current
                        rd_time_flag = True # set flag
                        time_skip_ctr = 0 # reset time_step_skip_counter
                        # print('debug: time registered')
                # condition for total number of atoms in the frame, this
                if (line[itm_pos+1] == 'NUMBER') and (not rd_natm_flag) and rd_time_flag:
                    n_atm = int(fp.readline().split()[0])
                    rd_natm_flag = True
                # condition for bounding box
                if (line[itm_pos+1] == 'BOX' and line[itm_pos+2] == 'BOUNDS') and (not box_dim_flag) and rd_time_flag:
                    # check if the bounding box is cubic format
                    if (line[itm_pos+3]=='pp' and line[itm_pos+4]=='pp' and line[itm_pos+5]=='pp'):
                        box_dims = \
                        read_lammps_box_cubic([fp.readline(), fp.readline(), fp.readline()])    # fp current line will be changed
                                                                # in the function
                        box_dim_flag = True
                    else:
                        warnings.warn(f'Non-cubic crystal type detected at frame {current_time},\
                         current not supported.')
                        break
                # condition for atom list
                if (line[itm_pos+1] == 'ATOMS') and (not rd_pos_flag) and (box_dim_flag) and\
                    (rd_natm_flag) and (rd_time_flag):
                    try:
                        atom_id_pos = line[itm_pos+2:].index('id')
                    except:
                        warnings.warn('Cannot find id field for atom. stop reading file')
                        break
                    try:
                        atom_type_pos = line[itm_pos+2:].index('type')
                    except:
                        try:
                            atom_type_pos = line[itm_pos+2:].index('element')
                        except:
                            warnings.warn('Cannot find type field for atom. stop reading file')
                            break
                    if len(extra_ppt) > 0: # if the list of extra properties has item(s)
                        ext_itm_ls = []
                        for ext_itm in extra_ppt:
                            try:
                                ext_itm_ls.append(line[itm_pos+2:].index(ext_itm))
                            except:
                                warnings.warn(f'{ext_itm} not in the LAMMPS data file.')
                    # read atom position flag, determine un/scale and un/wrapped.
                    if ('ix' in line) and ('iy' in line) and ('iz' in line):
                        atom_ix_pos = line[itm_pos+2:].index('ix')
                        atom_iy_pos = line[itm_pos+2:].index('iy')
                        atom_iz_pos = line[itm_pos+2:].index('iz')
                        ixyz = [atom_ix_pos, atom_iy_pos, atom_iz_pos]
                    else:
                        ixyz = False
                        warnings.warn('ix, iy and/or iz is not in the input file,\
                        unwrapping from wrapped input not available.')

                    if ('x' in line) and ('y' in line) and ('z' in line):
                        pos_mod = 'unscale'
                        atom_x_pos = line[itm_pos+2:].index('x')
                        atom_y_pos = line[itm_pos+2:].index('y')
                        atom_z_pos = line[itm_pos+2:].index('z')
                        pos_idx = [atom_x_pos, atom_y_pos, atom_z_pos]
                    elif ('xs' in line) and ('ys' in line) and  ('zs' in line):
                        pos_mod = 'scaled'
                        atom_xs_pos = line[itm_pos+2:].index('xs')
                        atom_ys_pos = line[itm_pos+2:].index('ys')
                        atom_zs_pos = line[itm_pos+2:].index('zs')
                        pos_idx = [atom_xs_pos, atom_ys_pos, atom_zs_pos]
                    elif ('xu' in line) and ('yu' in line) and  ('zu' in line):
                        pos_mod = 'unwrapped'
                        atom_xu_pos = line[itm_pos+2:].index('xu')
                        atom_yu_pos = line[itm_pos+2:].index('yu')
                        atom_zu_pos = line[itm_pos+2:].index('zu')
                        pos_idx = [atom_xu_pos, atom_yu_pos, atom_zu_pos]
                    elif ('xsu' in line) and ('ysu' in line) and ('zsu' in line):
                        pos_mod = 'scaleunwrapped'
                        atom_xsu_pos = line[itm_pos+2:].index('xsu')
                        atom_ysu_pos = line[itm_pos+2:].index('ysu')
                        atom_zsu_pos = line[itm_pos+2:].index('zsu')
                        pos_idx = [atom_xsu_pos, atom_ysu_pos, atom_zsu_pos]
                    else:
                        # raise ValueError('atom position mode cannot be recognized.\
                        #                 Must be x, xs, xu, or xsu')
                        warnings.warn('atom position mode cannot be recognized.\
                                        I.e., not one of x, xs, xu, or xsu')
                        print(f'Atom position mode cannot be recognized,\
                                skip time frame {current_time}.')
                        continue


############################
#### for debug purpose
    # time_arr = []
    # pos_arr = {}
    # ppt_arr = {}
    # for atm in atom_to_read:
    #     pos_arr[atm] = []
    #     ppt_arr[atm] = []


                    if read_line_mode == 'atm': # read atoms mode
                        for rdd in range(n_atm): # iterate the lines in the following, but will break if number of atoms to read reached.
                            line = fp.readline().split()
                            if int(line[atom_id_pos]) in atom_to_read: # if the atom ID exist in the list of atoms to be read.
                                # read_lammps_atmpos_line
                                # pos_arr[int(line[0])].append(atmposscl)
                                pos_arr[int(line[atom_id_pos])].append(\
                                read_lammps_atmpos_line(line, pos_mod, box_dims, wrapped_out, scaled_out, pos_idx, ixyz,))
                                rd_atom_ctr += 1
                                if rd_atom_ctr == len(atom_to_read):
                                    rd_time_flag = False
                                    rd_pos_flag = False
                                    box_dim_flag = False
                                    rd_natm_flag = False
                                    rd_atom_ctr = 0
                                    time_arr.append(current_time) # record time
                                    # continue
                                    break
                        # line = fp.readline()
                                    # crt_atom = int(line[0])
                            # crt_atom = int(line[0])

                    elif read_line_mode == 'ele': # if read element
                        pos_in_frm = {}
                        ppt_in_frm = {}
                        for elmt in element_to_read:
                            # pos_in_frm[elmt] = []
                            pos_in_frm[elmt] = {}
                        for rdd in range(n_atm):
                            line = fp.readline().split()
        #                     if int(line[1]) == ele_to_rd:
                            try:
                                crt_ele = int(line[atom_type_pos])
                            except:
                                crt_ele = line[atom_type_pos]
                            # if int(line[atom_type_pos]) in element_to_read:
                            if crt_ele in element_to_read:
                                # atmposscl = np.array(line[3:6], dtype=float)
                                atmposscl =\
                                read_lammps_atmpos_line(line, pos_mod, box_dims, wrapped_out, scaled_out, pos_idx, ixyz,)
                                # pos_in_frm[int(line[atom_type_pos])].append(atmposscl)
                                # pos_in_frm[crt_ele].append(atmposscl)
                                pos_in_frm[crt_ele][int(line[atom_id_pos])] = atmposscl

                        for elmt in element_to_read:
                            pos_arr[elmt].append(pos_in_frm[elmt])
                        rd_time_flag = False
                        rd_pos_flag = False
                        box_dim_flag = False
                        rd_natm_flag = False
                        time_arr.append(current_time) # record time
                        # line = fp.readline()
                    else:
                        warnings.warn('Read mode undefined.')
            line = fp.readline()
    return (time_arr, pos_arr)
